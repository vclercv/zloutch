# coding: utf-8
import unittest

from zloutch.roll import Action, RollSequence


class RollSequenceTestCase(unittest.TestCase):
    def setUp(self):
        self.roll_sequence = RollSequence()

    def test_roll(self):
        self.roll_sequence.frozen_dice = [1, 1, 1]
        self.roll_sequence.roll()

        self.assertEqual(len(self.roll_sequence.current_roll), 2)

        self.roll_sequence.frozen_dice = []
        self.roll_sequence.roll()

        self.assertEqual(len(self.roll_sequence.current_roll), 5)

    def test_update_state(self):
        # Test straights
        self.roll_sequence.current_roll = [3, 2, 1, 5, 4]
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 1500)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [])

        self.roll_sequence.current_roll = [3, 2, 6, 5, 4]
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 3000)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [])

        # Test 3 of a kind
        self.roll_sequence.current_roll = [1, 2, 1, 2, 1]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 1000)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL, Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [1, 1, 1])

        self.roll_sequence.current_roll = [3, 2, 3, 3, 6]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 300)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL, Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [3, 3, 3])

        # Test 4 of a kind
        self.roll_sequence.current_roll = [1, 1, 1, 2, 1]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 2000)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL, Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [1, 1, 1, 1])

        self.roll_sequence.current_roll = [6, 2, 6, 6, 6]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 1200)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL, Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [6, 6, 6, 6])

        # Test 5 of a kind
        self.roll_sequence.current_roll = [1, 1, 1, 1, 1]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 4000)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [])

        self.roll_sequence.current_roll = [5, 5, 5, 5, 5]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 2000)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [])

        # Mix
        self.roll_sequence.current_roll = [2, 2, 1, 2, 1]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 400)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [])

        # end with 50 -> ROLL
        self.roll_sequence.current_roll = [1, 2, 5, 1, 1]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 1050)
        self.assertEqual(self.roll_sequence.coche, 0)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.ROLL})
        self.assertEqual(self.roll_sequence.frozen_dice, [1, 1, 1, 5])

        # blank
        self.roll_sequence.current_roll = [2]
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 1050)
        self.assertEqual(self.roll_sequence.coche, 1)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [1, 1, 1, 5])

        # end with 50 -> ROLL
        self.roll_sequence.current_roll = [4, 2, 6, 6, 4]
        self.roll_sequence.score = 0
        self.roll_sequence.frozen_dice = []
        self.roll_sequence.update_state()
        self.assertEqual(self.roll_sequence.score, 0)
        self.assertEqual(self.roll_sequence.coche, 1)
        self.assertEqual(self.roll_sequence.valid_actions, {Action.STOP})
        self.assertEqual(self.roll_sequence.frozen_dice, [])


if __name__ == '__main__':
    unittest.main()
