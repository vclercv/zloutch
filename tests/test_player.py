# coding: utf-8
import unittest
from unittest.mock import MagicMock

from zloutch.player import ScoreLine, Player


class PlayerTestCase(unittest.TestCase):
    def setUp(self):

        self.player_0 = Player("Test Player 0")
        self.player_0.score_history = [ScoreLine(0, 0)]

        self.player_1 = Player("Test Player 1")
        self.player_1.score_history = [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(2000, 1)]

        self.player_2 = Player("Test Player 2")
        self.player_2.score_history = [ScoreLine(0, 0), ScoreLine(1000, 3), ScoreLine(1500, 3), ScoreLine(2000, 2)]

        self.player_3 = Player("Test Player 3")
        self.player_3.score_history = [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(1500, 1), ScoreLine(2000, 3)]
        self.player_3.score_index = -2

    def test_properties(self):
        self.assertEqual(self.player_0.score, 0)
        self.assertEqual(self.player_0.coches, 0)

        self.assertEqual(self.player_1.score, 2000)
        self.assertEqual(self.player_1.coches, 1)

        self.assertEqual(self.player_3.score, 1500)
        self.assertEqual(self.player_3.coches, 1)

    def test_get_fall_index(self):
        self.assertEqual(self.player_0.get_fall_index(), -1)
        self.assertEqual(self.player_1.get_fall_index(), -2)
        self.assertEqual(self.player_2.get_fall_index(), -4)
        self.assertEqual(self.player_3.get_fall_index(), -3)

    def test_add_coche(self):
        # Test for score 0
        self.player_0.add_coche()
        self.assertEqual(self.player_0.score_history, [ScoreLine(0, 0)])
        self.assertEqual(self.player_0.score_index, -1)

        # Test general case
        self.player_1.add_coche()
        self.assertListEqual(self.player_1.score_history,
                             [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(2000, 2)])
        self.assertEqual(self.player_1.score_index, -1)

        self.player_1.add_coche()
        self.assertListEqual(self.player_1.score_history,
                             [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(2000, 3)])
        self.assertEqual(self.player_1.score_index, -2)

        # Test multiple fall

        self.player_2.add_coche()
        self.assertListEqual(self.player_2.score_history,
                             [ScoreLine(0, 0), ScoreLine(1000, 3), ScoreLine(1500, 3), ScoreLine(2000, 3)])
        self.assertEqual(self.player_2.score_index, -4)

        # Test fall from middle

        self.player_3.add_coche()
        self.assertListEqual(self.player_3.score_history,
                             [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(1500, 2), ScoreLine(2000, 3)])
        self.assertEqual(self.player_3.score_index, -2)

        self.player_3.add_coche()
        self.assertListEqual(self.player_3.score_history,
                             [ScoreLine(0, 0), ScoreLine(1000, 0), ScoreLine(1500, 3), ScoreLine(2000, 3)])
        self.assertEqual(self.player_3.score_index, -3)

    def test_add_score(self):
        # Test case to enter the game
        self.player_0.add_coche = MagicMock()

        self.player_0.add_score(100, 0)
        self.assertEqual(self.player_0.score, 0)
        self.assertEqual(self.player_0.score_history, [ScoreLine(0, 0)])
        self.assertEqual(self.player_0.score_index, -1)

        self.player_0.add_score(500, 0)
        self.assertEqual(self.player_0.score, 500)
        self.assertEqual(self.player_0.score_history,
                         [ScoreLine(0, 0), ScoreLine(500, 0)])
        self.assertEqual(self.player_0.score_index, -1)

        self.player_0.add_coche.assert_not_called()

        self.player_0.add_score(100, 0)
        self.assertEqual(self.player_0.score, 600)
        self.assertEqual(self.player_0.score_history,
                         [ScoreLine(0, 0), ScoreLine(500, 0), ScoreLine(600, 0)])
        self.assertEqual(self.player_0.score_index, -1)

        self.player_0.add_coche.assert_not_called()

        self.player_0.add_score(100, 1)

        self.assertEqual(self.player_0.score, 600)
        self.assertEqual(len(self.player_0.score_history), 3)
        self.assertEqual(self.player_0.score_index, -1)
        self.player_0.add_coche.assert_called()

        # test exceeding limit
        self.player_0.add_score(Player.GOAL, 0)

        self.assertEqual(self.player_0.score, 600)
        self.assertEqual(len(self.player_0.score_history), 3)
        self.assertEqual(self.player_0.score_index, -1)
        self.player_0.add_coche.assert_called()


class ScoreLineTestCase(unittest.TestCase):
    def test_eq(self):
        self.assertEqual(ScoreLine(1000, 0), ScoreLine(1000, 0))
        self.assertNotEqual(ScoreLine(1000, 0), ScoreLine(1000, 1))
        self.assertNotEqual(ScoreLine(1000, 0), ScoreLine(2000, 0))


if __name__ == '__main__':
    unittest.main()
