# coding: utf-8

class ValidatedInput:
    @property
    def text(self):
        raise NotImplementedError

    def validate(self, value):
        raise NotImplementedError

    def input(self):
        output = None
        while output is None:
            value = input(self.text)
            output = self.validate(value)
            if output is None:
                print("Invalid input")
        return output


class PositiveIntegerInput(ValidatedInput):
    def __init__(self, text):
        self._text = text

    @property
    def text(self):
        return self._text

    def validate(self, value):
        return int(value) if value.isdigit() and int(value) > 0 else None


class MultipleChoiceInput(ValidatedInput):
    def __init__(self, choice_dict, action_validator):
        self.action_validator = action_validator
        self.choice_dict = choice_dict

    @property
    def text(self):
        return " or ".join(f"({k if self.action_validator(v[1]) else '*'}) {v[0]}"
                           for (k, v) in self.choice_dict.items()) + "?"

    def validate(self, value):
        action = self.choice_dict.get(value, (None, None))[1]

        if not self.action_validator(action):
            action = None

        return action
