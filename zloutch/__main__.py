# coding: utf-8
from zloutch.game import GameManager

if __name__ == '__main__':
    g = GameManager.interactive_setup()
    g.start()
