# coding: utf-8
from functools import partial

from zloutch.player import Player
from zloutch.roll import Action, RollSequence
from zloutch.utils import PositiveIntegerInput, MultipleChoiceInput


class GameManager:

    def __init__(self, player_count):
        self.player_count = player_count
        self.players = [Player(f"Player {i}") for i in range(self.player_count)]

        self.active_player_index = 0

        self.roll_sequence = RollSequence()

        self.after_roll_choice = MultipleChoiceInput({"1": ("roll", Action.ROLL),
                                                      "2": ("stop", Action.STOP)},
                                                     self.is_valid_action)

        self.player_change_input = MultipleChoiceInput({"1": ("continue", Action.ROLL),
                                                        "2": ("reset rolls", Action.STOP)}, self.is_valid_action)

    @staticmethod
    def interactive_setup():
        player_count_input = PositiveIntegerInput("How many players ?")
        player_count = player_count_input.input()
        return GameManager(player_count)

    @property
    def active_player(self):
        return self.players[self.active_player_index]

    def change_player(self):
        self.active_player_index = (self.active_player_index + 1) % self.player_count

    def print_change_turn(self):
        print(" {} ".format(self.active_player).center(80, "*"))

    def is_valid_action(self, action):
        return action in self.roll_sequence.valid_actions

    def start(self):
        self.print_change_turn()
        self.main_loop()

    def main_loop(self):
        while True:
            action = self.after_roll_choice.input()
            if action == Action.STOP:
                self.active_player.add_score(self.roll_sequence.score,
                                             self.roll_sequence.coche)
                print(self.active_player)
                if self.active_player.has_won():
                    break

                self.change_player()
                self.print_change_turn()
                # the new player can choose to continue on the previous sequence
                reset = self.player_change_input.input()
                if reset == action.STOP:
                    self.roll_sequence = RollSequence()
                else:
                    self.roll_sequence.disable_action(Action.STOP)
            elif action == Action.ROLL:
                self.roll_sequence.roll()
                print(self.roll_sequence)
        print("{} has won !!!".format(self.active_player.name))
