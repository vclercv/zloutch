# coding: utf-8
import random

from collections import Counter
from enum import Enum, auto


class Action(Enum):
    STOP = auto()
    ROLL = auto()


class RollSequence:

    multiplier_map = [0, 0, 0, 1, 2, 4]

    def __init__(self):
        self.frozen_dice = []
        self.current_roll = None
        self.score = 0
        self.coche = 0
        self.valid_actions = {Action.ROLL}

    def roll(self):
        self.current_roll = [random.randint(1, 6)
                             for i in range(5 - len(self.frozen_dice))]
        self.update_state()

    def update_state(self):
        dice_counter = Counter(self.current_roll)

        intermediate_score = 0

        used_dice = []

        # check the presence of a straight
        if len(dice_counter) == 5 and (dice_counter[1] == 0 or dice_counter[6] == 0):
            used_dice = self.current_roll
            intermediate_score += 1500
        else:
            # check if there is a three or more of a kind
            # there can't be 2 such sets as 3 + 3 > 5
            most_common_face, highest_frequency = dice_counter.most_common(1)[0]

            if highest_frequency >= 3:
                used_dice += [most_common_face] * highest_frequency
                if most_common_face == 1:
                    intermediate_score += 1000 * self.multiplier_map[highest_frequency]
                else:
                    intermediate_score += 100 * most_common_face \
                                          * self.multiplier_map[highest_frequency]

            # add the value of single 1s and 5s
            if dice_counter[1] <= 2:
                used_dice += [1] * dice_counter[1]
                intermediate_score += 100 * dice_counter[1]
            if dice_counter[5] <= 2:
                used_dice += [5] * dice_counter[5]
                intermediate_score += 50 * dice_counter[5]

        self.score += intermediate_score

        if intermediate_score == 0:
            self.valid_actions = {Action.STOP}
            self.coche = 1
            self.frozen_dice += used_dice
        # if all the dice are used, the player cannot stop
        elif len(used_dice) == len(self.current_roll):
            self.valid_actions = {Action.ROLL}
            self.frozen_dice = []
        # if score ends with 50 the player must continue
        elif self.score % 100 == 50:
            self.valid_actions = {Action.ROLL}
            self.frozen_dice += used_dice
        else:
            self.valid_actions = {Action.ROLL, Action.STOP}
            self.frozen_dice += used_dice

    def disable_action(self, action):
        self.valid_actions.remove(action)

    def __str__(self):
        return f"{self.current_roll} was rolled\n" \
               f"Frozen dice : {self.frozen_dice}, total: {self.score}"
