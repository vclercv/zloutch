# coding: utf-8

class ScoreLine:
    def __init__(self, points, coches):
        self.points = points
        self.coches = coches

    def __str__(self):
        return f"{self.points} {'|' * self.coches}"

    def __eq__(self, other):
        return self.points == other.points and self.coches == other.coches


class Player:

    GOAL = 10000
    MINIMAL_SCORE = 500

    def __init__(self, name):
        self.score_history = [ScoreLine(0, 0)]
        self.score_index = -1
        self.name = name

    @property
    def score(self):
        return self.score_history[self.score_index].points

    @property
    def coches(self):
        return self.score_history[self.score_index].coches

    @property
    def score_line(self):
        return self.score_history[self.score_index]

    def add_score(self, points, coche):
        if coche or (self.score + points) > Player.GOAL:
            self.add_coche()
        elif (points >= Player.MINIMAL_SCORE) or (self.score != 0):
            self.score_history.append(ScoreLine(self.score_line.points + points, 0))
            self.score_index = -1

    def add_coche(self):
        if self.score != 0:
            self.score_history[self.score_index].coches += 1
            if self.coches == 3:
                self.score_index = self.get_fall_index()

    def get_fall_index(self):
        if self.score == 0:
            return -len(self.score_history)
        else:
            idx = self.score_index - 1
            while self.score_history[idx].coches == 3:
                idx -= 1
            return idx

    def has_won(self):
        return self.score == Player.GOAL

    def __str__(self):
        return f"{self.name} score : {self.score_line}" \
               f"(falls to {self.score_history[self.get_fall_index()]})"
